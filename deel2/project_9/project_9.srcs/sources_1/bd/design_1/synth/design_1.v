//Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
//Date        : Wed Jan  2 16:41:29 2019
//Host        : DESKTOP-PC9UCAU running 64-bit major release  (build 9200)
//Command     : generate_target design_1.bd
//Design      : design_1
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "design_1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_1,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=5,numReposBlks=5,numNonXlnxBlks=1,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_IP}" *) (* HW_HANDOFF = "design_1.hwdef" *) 
module design_1
   ();

  wire Net;
  wire PmodWIFI_axi_quad_spi_0_0_SPI_0_IO0_I;
  wire PmodWIFI_axi_quad_spi_0_0_SPI_0_IO0_O;
  wire PmodWIFI_axi_quad_spi_0_0_SPI_0_IO0_T;
  wire PmodWIFI_axi_quad_spi_0_0_SPI_0_IO1_I;
  wire PmodWIFI_axi_quad_spi_0_0_SPI_0_IO1_O;
  wire PmodWIFI_axi_quad_spi_0_0_SPI_0_IO1_T;
  wire PmodWIFI_axi_quad_spi_0_0_SPI_0_SS_I;
  wire [0:0]PmodWIFI_axi_quad_spi_0_0_SPI_0_SS_O;
  wire PmodWIFI_axi_quad_spi_0_0_SPI_0_SS_T;

  design_1_axi_gpio_0_0 PmodWIFI_axi_gpio_0_0
       (.gpio_io_i(1'b0),
        .s_axi_aclk(Net),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_aresetn(1'b1),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bready(1'b0),
        .s_axi_rready(1'b0),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wstrb({1'b1,1'b1,1'b1,1'b1}),
        .s_axi_wvalid(1'b0));
  design_1_axi_gpio_0_1 PmodWIFI_axi_gpio_1_0
       (.gpio_io_i({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_aclk(Net),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_aresetn(1'b1),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bready(1'b0),
        .s_axi_rready(1'b0),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wstrb({1'b1,1'b1,1'b1,1'b1}),
        .s_axi_wvalid(1'b0));
  design_1_axi_quad_spi_0_0 PmodWIFI_axi_quad_spi_0_0
       (.ext_spi_clk(Net),
        .io0_i(PmodWIFI_axi_quad_spi_0_0_SPI_0_IO0_I),
        .io0_o(PmodWIFI_axi_quad_spi_0_0_SPI_0_IO0_O),
        .io0_t(PmodWIFI_axi_quad_spi_0_0_SPI_0_IO0_T),
        .io1_i(PmodWIFI_axi_quad_spi_0_0_SPI_0_IO1_I),
        .io1_o(PmodWIFI_axi_quad_spi_0_0_SPI_0_IO1_O),
        .io1_t(PmodWIFI_axi_quad_spi_0_0_SPI_0_IO1_T),
        .s_axi_aclk(Net),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_aresetn(1'b0),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bready(1'b0),
        .s_axi_rready(1'b0),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wstrb({1'b1,1'b1,1'b1,1'b1}),
        .s_axi_wvalid(1'b0),
        .ss_i(PmodWIFI_axi_quad_spi_0_0_SPI_0_SS_I),
        .ss_o(PmodWIFI_axi_quad_spi_0_0_SPI_0_SS_O),
        .ss_t(PmodWIFI_axi_quad_spi_0_0_SPI_0_SS_T));
  design_1_axi_timer_0_0 PmodWIFI_axi_timer_0_0
       (.capturetrig0(1'b0),
        .capturetrig1(1'b0),
        .freeze(1'b0),
        .generateout0(Net),
        .s_axi_aclk(Net),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_aresetn(1'b1),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bready(1'b0),
        .s_axi_rready(1'b0),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wstrb({1'b1,1'b1,1'b1,1'b1}),
        .s_axi_wvalid(1'b0));
  design_1_pmod_bridge_0_0 PmodWIFI_pmod_bridge_0_0
       (.in0_I(PmodWIFI_axi_quad_spi_0_0_SPI_0_SS_I),
        .in0_O(PmodWIFI_axi_quad_spi_0_0_SPI_0_SS_O),
        .in0_T(PmodWIFI_axi_quad_spi_0_0_SPI_0_SS_T),
        .in1_I(PmodWIFI_axi_quad_spi_0_0_SPI_0_IO0_I),
        .in1_O(PmodWIFI_axi_quad_spi_0_0_SPI_0_IO0_O),
        .in1_T(PmodWIFI_axi_quad_spi_0_0_SPI_0_IO0_T),
        .in2_I(PmodWIFI_axi_quad_spi_0_0_SPI_0_IO1_I),
        .in2_O(PmodWIFI_axi_quad_spi_0_0_SPI_0_IO1_O),
        .in2_T(PmodWIFI_axi_quad_spi_0_0_SPI_0_IO1_T),
        .in3_O(1'b1),
        .in3_T(1'b1),
        .in_bottom_bus_O({1'b0,1'b0,1'b0,1'b1}),
        .in_bottom_bus_T({1'b0,1'b0,1'b0,1'b1}),
        .out0_I(1'b0),
        .out1_I(1'b0),
        .out2_I(1'b0),
        .out3_I(1'b0),
        .out4_I(1'b0),
        .out5_I(1'b0),
        .out6_I(1'b0),
        .out7_I(1'b0));
endmodule
