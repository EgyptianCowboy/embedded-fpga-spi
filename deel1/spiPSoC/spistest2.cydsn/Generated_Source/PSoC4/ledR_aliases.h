/*******************************************************************************
* File Name: ledR.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_ledR_ALIASES_H) /* Pins ledR_ALIASES_H */
#define CY_PINS_ledR_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define ledR_0			(ledR__0__PC)
#define ledR_0_PS		(ledR__0__PS)
#define ledR_0_PC		(ledR__0__PC)
#define ledR_0_DR		(ledR__0__DR)
#define ledR_0_SHIFT	(ledR__0__SHIFT)
#define ledR_0_INTR	((uint16)((uint16)0x0003u << (ledR__0__SHIFT*2u)))

#define ledR_INTR_ALL	 ((uint16)(ledR_0_INTR))


#endif /* End Pins ledR_ALIASES_H */


/* [] END OF FILE */
