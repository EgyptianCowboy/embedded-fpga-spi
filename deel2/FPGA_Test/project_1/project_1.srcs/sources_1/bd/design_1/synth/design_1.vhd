--Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
--Date        : Mon Jan  7 17:15:30 2019
--Host        : DESKTOP-PC9UCAU running 64-bit major release  (build 9200)
--Command     : generate_target design_1.bd
--Design      : design_1
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1 is
  attribute CORE_GENERATION_INFO : string;
  attribute CORE_GENERATION_INFO of design_1 : entity is "design_1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_1,x_ipVersion=1.00.a,x_ipLanguage=VHDL,numBlks=5,numReposBlks=5,numNonXlnxBlks=1,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,da_board_cnt=8,synth_mode=OOC_per_IP}";
  attribute HW_HANDOFF : string;
  attribute HW_HANDOFF of design_1 : entity is "design_1.hwdef";
end design_1;

architecture STRUCTURE of design_1 is
  component design_1_axi_gpio_0_0 is
  port (
    s_axi_aclk : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 8 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 8 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    gpio_io_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    gpio_io_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    gpio_io_t : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_axi_gpio_0_0;
  component design_1_axi_gpio_0_1 is
  port (
    s_axi_aclk : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 8 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 8 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    ip2intc_irpt : out STD_LOGIC;
    gpio_io_o : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_axi_gpio_0_1;
  component design_1_axi_quad_spi_0_0 is
  port (
    ext_spi_clk : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 6 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 6 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    io0_i : in STD_LOGIC;
    io0_o : out STD_LOGIC;
    io0_t : out STD_LOGIC;
    io1_i : in STD_LOGIC;
    io1_o : out STD_LOGIC;
    io1_t : out STD_LOGIC;
    sck_i : in STD_LOGIC;
    sck_o : out STD_LOGIC;
    sck_t : out STD_LOGIC;
    ss_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    ss_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    ss_t : out STD_LOGIC;
    ip2intc_irpt : out STD_LOGIC
  );
  end component design_1_axi_quad_spi_0_0;
  component design_1_axi_timer_0_0 is
  port (
    capturetrig0 : in STD_LOGIC;
    capturetrig1 : in STD_LOGIC;
    generateout0 : out STD_LOGIC;
    generateout1 : out STD_LOGIC;
    pwm0 : out STD_LOGIC;
    interrupt : out STD_LOGIC;
    freeze : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC
  );
  end component design_1_axi_timer_0_0;
  component design_1_pmod_bridge_0_0 is
  port (
    in_bottom_bus_I : out STD_LOGIC_VECTOR ( 3 downto 0 );
    in_bottom_bus_O : in STD_LOGIC_VECTOR ( 3 downto 0 );
    in_bottom_bus_T : in STD_LOGIC_VECTOR ( 3 downto 0 );
    in0_I : out STD_LOGIC;
    in1_I : out STD_LOGIC;
    in2_I : out STD_LOGIC;
    in3_I : out STD_LOGIC;
    in0_O : in STD_LOGIC;
    in1_O : in STD_LOGIC;
    in2_O : in STD_LOGIC;
    in3_O : in STD_LOGIC;
    in0_T : in STD_LOGIC;
    in1_T : in STD_LOGIC;
    in2_T : in STD_LOGIC;
    in3_T : in STD_LOGIC;
    out0_I : in STD_LOGIC;
    out1_I : in STD_LOGIC;
    out2_I : in STD_LOGIC;
    out3_I : in STD_LOGIC;
    out4_I : in STD_LOGIC;
    out5_I : in STD_LOGIC;
    out6_I : in STD_LOGIC;
    out7_I : in STD_LOGIC;
    out0_O : out STD_LOGIC;
    out1_O : out STD_LOGIC;
    out2_O : out STD_LOGIC;
    out3_O : out STD_LOGIC;
    out4_O : out STD_LOGIC;
    out5_O : out STD_LOGIC;
    out6_O : out STD_LOGIC;
    out7_O : out STD_LOGIC;
    out0_T : out STD_LOGIC;
    out1_T : out STD_LOGIC;
    out2_T : out STD_LOGIC;
    out3_T : out STD_LOGIC;
    out4_T : out STD_LOGIC;
    out5_T : out STD_LOGIC;
    out6_T : out STD_LOGIC;
    out7_T : out STD_LOGIC
  );
  end component design_1_pmod_bridge_0_0;
  signal Net : STD_LOGIC;
  signal PmodWIFI_pmod_bridge_0_0_in0_I : STD_LOGIC;
  signal NLW_PmodWIFI_axi_gpio_0_0_s_axi_arready_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_gpio_0_0_s_axi_awready_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_gpio_0_0_s_axi_bvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_gpio_0_0_s_axi_rvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_gpio_0_0_s_axi_wready_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_gpio_0_0_gpio_io_o_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_PmodWIFI_axi_gpio_0_0_gpio_io_t_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_PmodWIFI_axi_gpio_0_0_s_axi_bresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_PmodWIFI_axi_gpio_0_0_s_axi_rdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_PmodWIFI_axi_gpio_0_0_s_axi_rresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_PmodWIFI_axi_gpio_1_0_ip2intc_irpt_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_gpio_1_0_s_axi_arready_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_gpio_1_0_s_axi_awready_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_gpio_1_0_s_axi_bvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_gpio_1_0_s_axi_rvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_gpio_1_0_s_axi_wready_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_gpio_1_0_gpio_io_o_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_PmodWIFI_axi_gpio_1_0_s_axi_bresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_PmodWIFI_axi_gpio_1_0_s_axi_rdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_PmodWIFI_axi_gpio_1_0_s_axi_rresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_PmodWIFI_axi_quad_spi_0_0_io0_o_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_quad_spi_0_0_io0_t_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_quad_spi_0_0_io1_o_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_quad_spi_0_0_io1_t_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_quad_spi_0_0_ip2intc_irpt_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_quad_spi_0_0_s_axi_arready_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_quad_spi_0_0_s_axi_awready_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_quad_spi_0_0_s_axi_bvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_quad_spi_0_0_s_axi_rvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_quad_spi_0_0_s_axi_wready_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_quad_spi_0_0_sck_o_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_quad_spi_0_0_sck_t_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_quad_spi_0_0_ss_t_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_quad_spi_0_0_s_axi_bresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_PmodWIFI_axi_quad_spi_0_0_s_axi_rdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_PmodWIFI_axi_quad_spi_0_0_s_axi_rresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_PmodWIFI_axi_quad_spi_0_0_ss_o_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_PmodWIFI_axi_timer_0_0_generateout0_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_timer_0_0_generateout1_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_timer_0_0_interrupt_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_timer_0_0_s_axi_arready_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_timer_0_0_s_axi_awready_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_timer_0_0_s_axi_bvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_timer_0_0_s_axi_rvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_timer_0_0_s_axi_wready_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_axi_timer_0_0_s_axi_bresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_PmodWIFI_axi_timer_0_0_s_axi_rdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_PmodWIFI_axi_timer_0_0_s_axi_rresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_PmodWIFI_pmod_bridge_0_0_in1_I_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_pmod_bridge_0_0_in2_I_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_pmod_bridge_0_0_in3_I_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_pmod_bridge_0_0_out0_O_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_pmod_bridge_0_0_out0_T_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_pmod_bridge_0_0_out1_O_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_pmod_bridge_0_0_out1_T_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_pmod_bridge_0_0_out2_O_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_pmod_bridge_0_0_out2_T_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_pmod_bridge_0_0_out3_O_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_pmod_bridge_0_0_out3_T_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_pmod_bridge_0_0_out4_O_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_pmod_bridge_0_0_out4_T_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_pmod_bridge_0_0_out5_O_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_pmod_bridge_0_0_out5_T_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_pmod_bridge_0_0_out6_O_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_pmod_bridge_0_0_out6_T_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_pmod_bridge_0_0_out7_O_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_pmod_bridge_0_0_out7_T_UNCONNECTED : STD_LOGIC;
  signal NLW_PmodWIFI_pmod_bridge_0_0_in_bottom_bus_I_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
PmodWIFI_axi_gpio_0_0: component design_1_axi_gpio_0_0
     port map (
      gpio_io_i(0) => PmodWIFI_pmod_bridge_0_0_in0_I,
      gpio_io_o(0) => NLW_PmodWIFI_axi_gpio_0_0_gpio_io_o_UNCONNECTED(0),
      gpio_io_t(0) => NLW_PmodWIFI_axi_gpio_0_0_gpio_io_t_UNCONNECTED(0),
      s_axi_aclk => Net,
      s_axi_araddr(8 downto 0) => B"000000000",
      s_axi_aresetn => '1',
      s_axi_arready => NLW_PmodWIFI_axi_gpio_0_0_s_axi_arready_UNCONNECTED,
      s_axi_arvalid => '0',
      s_axi_awaddr(8 downto 0) => B"000000000",
      s_axi_awready => NLW_PmodWIFI_axi_gpio_0_0_s_axi_awready_UNCONNECTED,
      s_axi_awvalid => '0',
      s_axi_bready => '0',
      s_axi_bresp(1 downto 0) => NLW_PmodWIFI_axi_gpio_0_0_s_axi_bresp_UNCONNECTED(1 downto 0),
      s_axi_bvalid => NLW_PmodWIFI_axi_gpio_0_0_s_axi_bvalid_UNCONNECTED,
      s_axi_rdata(31 downto 0) => NLW_PmodWIFI_axi_gpio_0_0_s_axi_rdata_UNCONNECTED(31 downto 0),
      s_axi_rready => '0',
      s_axi_rresp(1 downto 0) => NLW_PmodWIFI_axi_gpio_0_0_s_axi_rresp_UNCONNECTED(1 downto 0),
      s_axi_rvalid => NLW_PmodWIFI_axi_gpio_0_0_s_axi_rvalid_UNCONNECTED,
      s_axi_wdata(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_wready => NLW_PmodWIFI_axi_gpio_0_0_s_axi_wready_UNCONNECTED,
      s_axi_wstrb(3 downto 0) => B"1111",
      s_axi_wvalid => '0'
    );
PmodWIFI_axi_gpio_1_0: component design_1_axi_gpio_0_1
     port map (
      gpio_io_o(0) => NLW_PmodWIFI_axi_gpio_1_0_gpio_io_o_UNCONNECTED(0),
      ip2intc_irpt => NLW_PmodWIFI_axi_gpio_1_0_ip2intc_irpt_UNCONNECTED,
      s_axi_aclk => Net,
      s_axi_araddr(8 downto 0) => B"000000000",
      s_axi_aresetn => '1',
      s_axi_arready => NLW_PmodWIFI_axi_gpio_1_0_s_axi_arready_UNCONNECTED,
      s_axi_arvalid => '0',
      s_axi_awaddr(8 downto 0) => B"000000000",
      s_axi_awready => NLW_PmodWIFI_axi_gpio_1_0_s_axi_awready_UNCONNECTED,
      s_axi_awvalid => '0',
      s_axi_bready => '0',
      s_axi_bresp(1 downto 0) => NLW_PmodWIFI_axi_gpio_1_0_s_axi_bresp_UNCONNECTED(1 downto 0),
      s_axi_bvalid => NLW_PmodWIFI_axi_gpio_1_0_s_axi_bvalid_UNCONNECTED,
      s_axi_rdata(31 downto 0) => NLW_PmodWIFI_axi_gpio_1_0_s_axi_rdata_UNCONNECTED(31 downto 0),
      s_axi_rready => '0',
      s_axi_rresp(1 downto 0) => NLW_PmodWIFI_axi_gpio_1_0_s_axi_rresp_UNCONNECTED(1 downto 0),
      s_axi_rvalid => NLW_PmodWIFI_axi_gpio_1_0_s_axi_rvalid_UNCONNECTED,
      s_axi_wdata(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_wready => NLW_PmodWIFI_axi_gpio_1_0_s_axi_wready_UNCONNECTED,
      s_axi_wstrb(3 downto 0) => B"1111",
      s_axi_wvalid => '0'
    );
PmodWIFI_axi_quad_spi_0_0: component design_1_axi_quad_spi_0_0
     port map (
      ext_spi_clk => Net,
      io0_i => '0',
      io0_o => NLW_PmodWIFI_axi_quad_spi_0_0_io0_o_UNCONNECTED,
      io0_t => NLW_PmodWIFI_axi_quad_spi_0_0_io0_t_UNCONNECTED,
      io1_i => '0',
      io1_o => NLW_PmodWIFI_axi_quad_spi_0_0_io1_o_UNCONNECTED,
      io1_t => NLW_PmodWIFI_axi_quad_spi_0_0_io1_t_UNCONNECTED,
      ip2intc_irpt => NLW_PmodWIFI_axi_quad_spi_0_0_ip2intc_irpt_UNCONNECTED,
      s_axi_aclk => Net,
      s_axi_araddr(6 downto 0) => B"0000000",
      s_axi_aresetn => '0',
      s_axi_arready => NLW_PmodWIFI_axi_quad_spi_0_0_s_axi_arready_UNCONNECTED,
      s_axi_arvalid => '0',
      s_axi_awaddr(6 downto 0) => B"0000000",
      s_axi_awready => NLW_PmodWIFI_axi_quad_spi_0_0_s_axi_awready_UNCONNECTED,
      s_axi_awvalid => '0',
      s_axi_bready => '0',
      s_axi_bresp(1 downto 0) => NLW_PmodWIFI_axi_quad_spi_0_0_s_axi_bresp_UNCONNECTED(1 downto 0),
      s_axi_bvalid => NLW_PmodWIFI_axi_quad_spi_0_0_s_axi_bvalid_UNCONNECTED,
      s_axi_rdata(31 downto 0) => NLW_PmodWIFI_axi_quad_spi_0_0_s_axi_rdata_UNCONNECTED(31 downto 0),
      s_axi_rready => '0',
      s_axi_rresp(1 downto 0) => NLW_PmodWIFI_axi_quad_spi_0_0_s_axi_rresp_UNCONNECTED(1 downto 0),
      s_axi_rvalid => NLW_PmodWIFI_axi_quad_spi_0_0_s_axi_rvalid_UNCONNECTED,
      s_axi_wdata(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_wready => NLW_PmodWIFI_axi_quad_spi_0_0_s_axi_wready_UNCONNECTED,
      s_axi_wstrb(3 downto 0) => B"1111",
      s_axi_wvalid => '0',
      sck_i => '0',
      sck_o => NLW_PmodWIFI_axi_quad_spi_0_0_sck_o_UNCONNECTED,
      sck_t => NLW_PmodWIFI_axi_quad_spi_0_0_sck_t_UNCONNECTED,
      ss_i(0) => '0',
      ss_o(0) => NLW_PmodWIFI_axi_quad_spi_0_0_ss_o_UNCONNECTED(0),
      ss_t => NLW_PmodWIFI_axi_quad_spi_0_0_ss_t_UNCONNECTED
    );
PmodWIFI_axi_timer_0_0: component design_1_axi_timer_0_0
     port map (
      capturetrig0 => '0',
      capturetrig1 => '0',
      freeze => '0',
      generateout0 => NLW_PmodWIFI_axi_timer_0_0_generateout0_UNCONNECTED,
      generateout1 => NLW_PmodWIFI_axi_timer_0_0_generateout1_UNCONNECTED,
      interrupt => NLW_PmodWIFI_axi_timer_0_0_interrupt_UNCONNECTED,
      pwm0 => Net,
      s_axi_aclk => Net,
      s_axi_araddr(4 downto 0) => B"00000",
      s_axi_aresetn => '1',
      s_axi_arready => NLW_PmodWIFI_axi_timer_0_0_s_axi_arready_UNCONNECTED,
      s_axi_arvalid => '0',
      s_axi_awaddr(4 downto 0) => B"00000",
      s_axi_awready => NLW_PmodWIFI_axi_timer_0_0_s_axi_awready_UNCONNECTED,
      s_axi_awvalid => '0',
      s_axi_bready => '0',
      s_axi_bresp(1 downto 0) => NLW_PmodWIFI_axi_timer_0_0_s_axi_bresp_UNCONNECTED(1 downto 0),
      s_axi_bvalid => NLW_PmodWIFI_axi_timer_0_0_s_axi_bvalid_UNCONNECTED,
      s_axi_rdata(31 downto 0) => NLW_PmodWIFI_axi_timer_0_0_s_axi_rdata_UNCONNECTED(31 downto 0),
      s_axi_rready => '0',
      s_axi_rresp(1 downto 0) => NLW_PmodWIFI_axi_timer_0_0_s_axi_rresp_UNCONNECTED(1 downto 0),
      s_axi_rvalid => NLW_PmodWIFI_axi_timer_0_0_s_axi_rvalid_UNCONNECTED,
      s_axi_wdata(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_wready => NLW_PmodWIFI_axi_timer_0_0_s_axi_wready_UNCONNECTED,
      s_axi_wstrb(3 downto 0) => B"1111",
      s_axi_wvalid => '0'
    );
PmodWIFI_pmod_bridge_0_0: component design_1_pmod_bridge_0_0
     port map (
      in0_I => PmodWIFI_pmod_bridge_0_0_in0_I,
      in0_O => '1',
      in0_T => '1',
      in1_I => NLW_PmodWIFI_pmod_bridge_0_0_in1_I_UNCONNECTED,
      in1_O => '1',
      in1_T => '1',
      in2_I => NLW_PmodWIFI_pmod_bridge_0_0_in2_I_UNCONNECTED,
      in2_O => '1',
      in2_T => '1',
      in3_I => NLW_PmodWIFI_pmod_bridge_0_0_in3_I_UNCONNECTED,
      in3_O => '1',
      in3_T => '1',
      in_bottom_bus_I(3 downto 0) => NLW_PmodWIFI_pmod_bridge_0_0_in_bottom_bus_I_UNCONNECTED(3 downto 0),
      in_bottom_bus_O(3 downto 0) => B"0001",
      in_bottom_bus_T(3 downto 0) => B"0001",
      out0_I => '0',
      out0_O => NLW_PmodWIFI_pmod_bridge_0_0_out0_O_UNCONNECTED,
      out0_T => NLW_PmodWIFI_pmod_bridge_0_0_out0_T_UNCONNECTED,
      out1_I => '0',
      out1_O => NLW_PmodWIFI_pmod_bridge_0_0_out1_O_UNCONNECTED,
      out1_T => NLW_PmodWIFI_pmod_bridge_0_0_out1_T_UNCONNECTED,
      out2_I => '0',
      out2_O => NLW_PmodWIFI_pmod_bridge_0_0_out2_O_UNCONNECTED,
      out2_T => NLW_PmodWIFI_pmod_bridge_0_0_out2_T_UNCONNECTED,
      out3_I => '0',
      out3_O => NLW_PmodWIFI_pmod_bridge_0_0_out3_O_UNCONNECTED,
      out3_T => NLW_PmodWIFI_pmod_bridge_0_0_out3_T_UNCONNECTED,
      out4_I => '0',
      out4_O => NLW_PmodWIFI_pmod_bridge_0_0_out4_O_UNCONNECTED,
      out4_T => NLW_PmodWIFI_pmod_bridge_0_0_out4_T_UNCONNECTED,
      out5_I => '0',
      out5_O => NLW_PmodWIFI_pmod_bridge_0_0_out5_O_UNCONNECTED,
      out5_T => NLW_PmodWIFI_pmod_bridge_0_0_out5_T_UNCONNECTED,
      out6_I => '0',
      out6_O => NLW_PmodWIFI_pmod_bridge_0_0_out6_O_UNCONNECTED,
      out6_T => NLW_PmodWIFI_pmod_bridge_0_0_out6_T_UNCONNECTED,
      out7_I => '0',
      out7_O => NLW_PmodWIFI_pmod_bridge_0_0_out7_O_UNCONNECTED,
      out7_T => NLW_PmodWIFI_pmod_bridge_0_0_out7_T_UNCONNECTED
    );
end STRUCTURE;
