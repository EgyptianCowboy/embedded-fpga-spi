# This file is automatically generated.
# It contains project source information necessary for synthesis and implementation.

# IP: C:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/design_2_PmodWIFI_0_1.xci
# IP: The module: 'design_2_PmodWIFI_0_1' is the root of the design. Do not add the DONT_TOUCH constraint.

# IP: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_pmod_bridge_0_0/PmodWIFI_pmod_bridge_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==PmodWIFI_pmod_bridge_0_0 || ORIG_REF_NAME==PmodWIFI_pmod_bridge_0_0} -quiet] -quiet

# IP: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_quad_spi_0_0/PmodWIFI_axi_quad_spi_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==PmodWIFI_axi_quad_spi_0_0 || ORIG_REF_NAME==PmodWIFI_axi_quad_spi_0_0} -quiet] -quiet

# IP: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_gpio_0_0/PmodWIFI_axi_gpio_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==PmodWIFI_axi_gpio_0_0 || ORIG_REF_NAME==PmodWIFI_axi_gpio_0_0} -quiet] -quiet

# IP: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_gpio_1_0/PmodWIFI_axi_gpio_1_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==PmodWIFI_axi_gpio_1_0 || ORIG_REF_NAME==PmodWIFI_axi_gpio_1_0} -quiet] -quiet

# IP: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_timer_0_0/PmodWIFI_axi_timer_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==PmodWIFI_axi_timer_0_0 || ORIG_REF_NAME==PmodWIFI_axi_timer_0_0} -quiet] -quiet

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_pmod_bridge_0_0/PmodWIFI_pmod_bridge_0_0_board.xdc
set_property DONT_TOUCH TRUE [get_cells [split [join [get_cells -hier -filter {REF_NAME==PmodWIFI_pmod_bridge_0_0 || ORIG_REF_NAME==PmodWIFI_pmod_bridge_0_0} -quiet] {/inst } ]/inst ] -quiet] -quiet

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_pmod_bridge_0_0/src/pmod_concat_ooc.xdc

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_quad_spi_0_0/PmodWIFI_axi_quad_spi_0_0_board.xdc
set_property DONT_TOUCH TRUE [get_cells [split [join [get_cells -hier -filter {REF_NAME==PmodWIFI_axi_quad_spi_0_0 || ORIG_REF_NAME==PmodWIFI_axi_quad_spi_0_0} -quiet] {/U0 } ]/U0 ] -quiet] -quiet

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_quad_spi_0_0/PmodWIFI_axi_quad_spi_0_0.xdc
#dup# set_property DONT_TOUCH TRUE [get_cells [split [join [get_cells -hier -filter {REF_NAME==PmodWIFI_axi_quad_spi_0_0 || ORIG_REF_NAME==PmodWIFI_axi_quad_spi_0_0} -quiet] {/U0 } ]/U0 ] -quiet] -quiet

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_quad_spi_0_0/PmodWIFI_axi_quad_spi_0_0_ooc.xdc

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_quad_spi_0_0/PmodWIFI_axi_quad_spi_0_0_clocks.xdc
#dup# set_property DONT_TOUCH TRUE [get_cells [split [join [get_cells -hier -filter {REF_NAME==PmodWIFI_axi_quad_spi_0_0 || ORIG_REF_NAME==PmodWIFI_axi_quad_spi_0_0} -quiet] {/U0 } ]/U0 ] -quiet] -quiet

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_gpio_0_0/PmodWIFI_axi_gpio_0_0_board.xdc
set_property DONT_TOUCH TRUE [get_cells [split [join [get_cells -hier -filter {REF_NAME==PmodWIFI_axi_gpio_0_0 || ORIG_REF_NAME==PmodWIFI_axi_gpio_0_0} -quiet] {/U0 } ]/U0 ] -quiet] -quiet

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_gpio_0_0/PmodWIFI_axi_gpio_0_0_ooc.xdc

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_gpio_0_0/PmodWIFI_axi_gpio_0_0.xdc
#dup# set_property DONT_TOUCH TRUE [get_cells [split [join [get_cells -hier -filter {REF_NAME==PmodWIFI_axi_gpio_0_0 || ORIG_REF_NAME==PmodWIFI_axi_gpio_0_0} -quiet] {/U0 } ]/U0 ] -quiet] -quiet

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_gpio_1_0/PmodWIFI_axi_gpio_1_0_board.xdc
set_property DONT_TOUCH TRUE [get_cells [split [join [get_cells -hier -filter {REF_NAME==PmodWIFI_axi_gpio_1_0 || ORIG_REF_NAME==PmodWIFI_axi_gpio_1_0} -quiet] {/U0 } ]/U0 ] -quiet] -quiet

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_gpio_1_0/PmodWIFI_axi_gpio_1_0_ooc.xdc

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_gpio_1_0/PmodWIFI_axi_gpio_1_0.xdc
#dup# set_property DONT_TOUCH TRUE [get_cells [split [join [get_cells -hier -filter {REF_NAME==PmodWIFI_axi_gpio_1_0 || ORIG_REF_NAME==PmodWIFI_axi_gpio_1_0} -quiet] {/U0 } ]/U0 ] -quiet] -quiet

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_timer_0_0/PmodWIFI_axi_timer_0_0.xdc
set_property DONT_TOUCH TRUE [get_cells [split [join [get_cells -hier -filter {REF_NAME==PmodWIFI_axi_timer_0_0 || ORIG_REF_NAME==PmodWIFI_axi_timer_0_0} -quiet] {/U0 } ]/U0 ] -quiet] -quiet

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_timer_0_0/PmodWIFI_axi_timer_0_0_ooc.xdc

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/design_2_PmodWIFI_0_1_board.xdc
# XDC: The top module name and the constraint reference have the same name: 'design_2_PmodWIFI_0_1'. Do not add the DONT_TOUCH constraint.
set_property DONT_TOUCH TRUE [get_cells inst -quiet] -quiet

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_ooc.xdc
# XDC: The top module name and the constraint reference have the same name: 'design_2_PmodWIFI_0_1'. Do not add the DONT_TOUCH constraint.
#dup# set_property DONT_TOUCH TRUE [get_cells inst -quiet] -quiet

# IP: C:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/design_2_PmodWIFI_0_1.xci
# IP: The module: 'design_2_PmodWIFI_0_1' is the root of the design. Do not add the DONT_TOUCH constraint.

# IP: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_pmod_bridge_0_0/PmodWIFI_pmod_bridge_0_0.xci
#dup# set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==PmodWIFI_pmod_bridge_0_0 || ORIG_REF_NAME==PmodWIFI_pmod_bridge_0_0} -quiet] -quiet

# IP: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_quad_spi_0_0/PmodWIFI_axi_quad_spi_0_0.xci
#dup# set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==PmodWIFI_axi_quad_spi_0_0 || ORIG_REF_NAME==PmodWIFI_axi_quad_spi_0_0} -quiet] -quiet

# IP: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_gpio_0_0/PmodWIFI_axi_gpio_0_0.xci
#dup# set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==PmodWIFI_axi_gpio_0_0 || ORIG_REF_NAME==PmodWIFI_axi_gpio_0_0} -quiet] -quiet

# IP: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_gpio_1_0/PmodWIFI_axi_gpio_1_0.xci
#dup# set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==PmodWIFI_axi_gpio_1_0 || ORIG_REF_NAME==PmodWIFI_axi_gpio_1_0} -quiet] -quiet

# IP: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_timer_0_0/PmodWIFI_axi_timer_0_0.xci
#dup# set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==PmodWIFI_axi_timer_0_0 || ORIG_REF_NAME==PmodWIFI_axi_timer_0_0} -quiet] -quiet

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_pmod_bridge_0_0/PmodWIFI_pmod_bridge_0_0_board.xdc
#dup# set_property DONT_TOUCH TRUE [get_cells [split [join [get_cells -hier -filter {REF_NAME==PmodWIFI_pmod_bridge_0_0 || ORIG_REF_NAME==PmodWIFI_pmod_bridge_0_0} -quiet] {/inst } ]/inst ] -quiet] -quiet

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_pmod_bridge_0_0/src/pmod_concat_ooc.xdc

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_quad_spi_0_0/PmodWIFI_axi_quad_spi_0_0_board.xdc
#dup# set_property DONT_TOUCH TRUE [get_cells [split [join [get_cells -hier -filter {REF_NAME==PmodWIFI_axi_quad_spi_0_0 || ORIG_REF_NAME==PmodWIFI_axi_quad_spi_0_0} -quiet] {/U0 } ]/U0 ] -quiet] -quiet

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_quad_spi_0_0/PmodWIFI_axi_quad_spi_0_0.xdc
#dup# set_property DONT_TOUCH TRUE [get_cells [split [join [get_cells -hier -filter {REF_NAME==PmodWIFI_axi_quad_spi_0_0 || ORIG_REF_NAME==PmodWIFI_axi_quad_spi_0_0} -quiet] {/U0 } ]/U0 ] -quiet] -quiet

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_quad_spi_0_0/PmodWIFI_axi_quad_spi_0_0_ooc.xdc

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_quad_spi_0_0/PmodWIFI_axi_quad_spi_0_0_clocks.xdc
#dup# set_property DONT_TOUCH TRUE [get_cells [split [join [get_cells -hier -filter {REF_NAME==PmodWIFI_axi_quad_spi_0_0 || ORIG_REF_NAME==PmodWIFI_axi_quad_spi_0_0} -quiet] {/U0 } ]/U0 ] -quiet] -quiet

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_gpio_0_0/PmodWIFI_axi_gpio_0_0_board.xdc
#dup# set_property DONT_TOUCH TRUE [get_cells [split [join [get_cells -hier -filter {REF_NAME==PmodWIFI_axi_gpio_0_0 || ORIG_REF_NAME==PmodWIFI_axi_gpio_0_0} -quiet] {/U0 } ]/U0 ] -quiet] -quiet

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_gpio_0_0/PmodWIFI_axi_gpio_0_0_ooc.xdc

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_gpio_0_0/PmodWIFI_axi_gpio_0_0.xdc
#dup# set_property DONT_TOUCH TRUE [get_cells [split [join [get_cells -hier -filter {REF_NAME==PmodWIFI_axi_gpio_0_0 || ORIG_REF_NAME==PmodWIFI_axi_gpio_0_0} -quiet] {/U0 } ]/U0 ] -quiet] -quiet

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_gpio_1_0/PmodWIFI_axi_gpio_1_0_board.xdc
#dup# set_property DONT_TOUCH TRUE [get_cells [split [join [get_cells -hier -filter {REF_NAME==PmodWIFI_axi_gpio_1_0 || ORIG_REF_NAME==PmodWIFI_axi_gpio_1_0} -quiet] {/U0 } ]/U0 ] -quiet] -quiet

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_gpio_1_0/PmodWIFI_axi_gpio_1_0_ooc.xdc

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_gpio_1_0/PmodWIFI_axi_gpio_1_0.xdc
#dup# set_property DONT_TOUCH TRUE [get_cells [split [join [get_cells -hier -filter {REF_NAME==PmodWIFI_axi_gpio_1_0 || ORIG_REF_NAME==PmodWIFI_axi_gpio_1_0} -quiet] {/U0 } ]/U0 ] -quiet] -quiet

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_timer_0_0/PmodWIFI_axi_timer_0_0.xdc
#dup# set_property DONT_TOUCH TRUE [get_cells [split [join [get_cells -hier -filter {REF_NAME==PmodWIFI_axi_timer_0_0 || ORIG_REF_NAME==PmodWIFI_axi_timer_0_0} -quiet] {/U0 } ]/U0 ] -quiet] -quiet

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_axi_timer_0_0/PmodWIFI_axi_timer_0_0_ooc.xdc

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/design_2_PmodWIFI_0_1_board.xdc
# XDC: The top module name and the constraint reference have the same name: 'design_2_PmodWIFI_0_1'. Do not add the DONT_TOUCH constraint.
#dup# set_property DONT_TOUCH TRUE [get_cells inst -quiet] -quiet

# XDC: c:/Users/Gebruiker/Documents/pxl/FPGA/project_5/project_5.srcs/sources_1/bd/design_2/ip/design_2_PmodWIFI_0_1/src/PmodWIFI_ooc.xdc
# XDC: The top module name and the constraint reference have the same name: 'design_2_PmodWIFI_0_1'. Do not add the DONT_TOUCH constraint.
#dup# set_property DONT_TOUCH TRUE [get_cells inst -quiet] -quiet
