/*******************************************************************************
* File Name: ledR.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_ledR_H) /* Pins ledR_H */
#define CY_PINS_ledR_H

#include "cytypes.h"
#include "cyfitter.h"
#include "ledR_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} ledR_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   ledR_Read(void);
void    ledR_Write(uint8 value);
uint8   ledR_ReadDataReg(void);
#if defined(ledR__PC) || (CY_PSOC4_4200L) 
    void    ledR_SetDriveMode(uint8 mode);
#endif
void    ledR_SetInterruptMode(uint16 position, uint16 mode);
uint8   ledR_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void ledR_Sleep(void); 
void ledR_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(ledR__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define ledR_DRIVE_MODE_BITS        (3)
    #define ledR_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - ledR_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the ledR_SetDriveMode() function.
         *  @{
         */
        #define ledR_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define ledR_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define ledR_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define ledR_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define ledR_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define ledR_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define ledR_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define ledR_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define ledR_MASK               ledR__MASK
#define ledR_SHIFT              ledR__SHIFT
#define ledR_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in ledR_SetInterruptMode() function.
     *  @{
     */
        #define ledR_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define ledR_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define ledR_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define ledR_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(ledR__SIO)
    #define ledR_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(ledR__PC) && (CY_PSOC4_4200L)
    #define ledR_USBIO_ENABLE               ((uint32)0x80000000u)
    #define ledR_USBIO_DISABLE              ((uint32)(~ledR_USBIO_ENABLE))
    #define ledR_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define ledR_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define ledR_USBIO_ENTER_SLEEP          ((uint32)((1u << ledR_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << ledR_USBIO_SUSPEND_DEL_SHIFT)))
    #define ledR_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << ledR_USBIO_SUSPEND_SHIFT)))
    #define ledR_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << ledR_USBIO_SUSPEND_DEL_SHIFT)))
    #define ledR_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(ledR__PC)
    /* Port Configuration */
    #define ledR_PC                 (* (reg32 *) ledR__PC)
#endif
/* Pin State */
#define ledR_PS                     (* (reg32 *) ledR__PS)
/* Data Register */
#define ledR_DR                     (* (reg32 *) ledR__DR)
/* Input Buffer Disable Override */
#define ledR_INP_DIS                (* (reg32 *) ledR__PC2)

/* Interrupt configuration Registers */
#define ledR_INTCFG                 (* (reg32 *) ledR__INTCFG)
#define ledR_INTSTAT                (* (reg32 *) ledR__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define ledR_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(ledR__SIO)
    #define ledR_SIO_REG            (* (reg32 *) ledR__SIO)
#endif /* (ledR__SIO_CFG) */

/* USBIO registers */
#if !defined(ledR__PC) && (CY_PSOC4_4200L)
    #define ledR_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define ledR_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define ledR_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define ledR_DRIVE_MODE_SHIFT       (0x00u)
#define ledR_DRIVE_MODE_MASK        (0x07u << ledR_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins ledR_H */


/* [] END OF FILE */
