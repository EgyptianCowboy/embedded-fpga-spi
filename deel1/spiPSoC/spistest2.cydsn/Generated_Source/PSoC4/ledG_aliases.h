/*******************************************************************************
* File Name: ledG.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_ledG_ALIASES_H) /* Pins ledG_ALIASES_H */
#define CY_PINS_ledG_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define ledG_0			(ledG__0__PC)
#define ledG_0_PS		(ledG__0__PS)
#define ledG_0_PC		(ledG__0__PC)
#define ledG_0_DR		(ledG__0__DR)
#define ledG_0_SHIFT	(ledG__0__SHIFT)
#define ledG_0_INTR	((uint16)((uint16)0x0003u << (ledG__0__SHIFT*2u)))

#define ledG_INTR_ALL	 ((uint16)(ledG_0_INTR))


#endif /* End Pins ledG_ALIASES_H */


/* [] END OF FILE */
