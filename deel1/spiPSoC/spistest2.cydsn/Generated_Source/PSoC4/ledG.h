/*******************************************************************************
* File Name: ledG.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_ledG_H) /* Pins ledG_H */
#define CY_PINS_ledG_H

#include "cytypes.h"
#include "cyfitter.h"
#include "ledG_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} ledG_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   ledG_Read(void);
void    ledG_Write(uint8 value);
uint8   ledG_ReadDataReg(void);
#if defined(ledG__PC) || (CY_PSOC4_4200L) 
    void    ledG_SetDriveMode(uint8 mode);
#endif
void    ledG_SetInterruptMode(uint16 position, uint16 mode);
uint8   ledG_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void ledG_Sleep(void); 
void ledG_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(ledG__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define ledG_DRIVE_MODE_BITS        (3)
    #define ledG_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - ledG_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the ledG_SetDriveMode() function.
         *  @{
         */
        #define ledG_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define ledG_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define ledG_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define ledG_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define ledG_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define ledG_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define ledG_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define ledG_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define ledG_MASK               ledG__MASK
#define ledG_SHIFT              ledG__SHIFT
#define ledG_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in ledG_SetInterruptMode() function.
     *  @{
     */
        #define ledG_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define ledG_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define ledG_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define ledG_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(ledG__SIO)
    #define ledG_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(ledG__PC) && (CY_PSOC4_4200L)
    #define ledG_USBIO_ENABLE               ((uint32)0x80000000u)
    #define ledG_USBIO_DISABLE              ((uint32)(~ledG_USBIO_ENABLE))
    #define ledG_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define ledG_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define ledG_USBIO_ENTER_SLEEP          ((uint32)((1u << ledG_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << ledG_USBIO_SUSPEND_DEL_SHIFT)))
    #define ledG_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << ledG_USBIO_SUSPEND_SHIFT)))
    #define ledG_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << ledG_USBIO_SUSPEND_DEL_SHIFT)))
    #define ledG_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(ledG__PC)
    /* Port Configuration */
    #define ledG_PC                 (* (reg32 *) ledG__PC)
#endif
/* Pin State */
#define ledG_PS                     (* (reg32 *) ledG__PS)
/* Data Register */
#define ledG_DR                     (* (reg32 *) ledG__DR)
/* Input Buffer Disable Override */
#define ledG_INP_DIS                (* (reg32 *) ledG__PC2)

/* Interrupt configuration Registers */
#define ledG_INTCFG                 (* (reg32 *) ledG__INTCFG)
#define ledG_INTSTAT                (* (reg32 *) ledG__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define ledG_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(ledG__SIO)
    #define ledG_SIO_REG            (* (reg32 *) ledG__SIO)
#endif /* (ledG__SIO_CFG) */

/* USBIO registers */
#if !defined(ledG__PC) && (CY_PSOC4_4200L)
    #define ledG_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define ledG_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define ledG_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define ledG_DRIVE_MODE_SHIFT       (0x00u)
#define ledG_DRIVE_MODE_MASK        (0x07u << ledG_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins ledG_H */


/* [] END OF FILE */
