# To ARD_D0 on Arduino 8-pin  Pin 1 = Slave Select
set_property PACKAGE_PIN R8 [get_ports {ss_o[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {ss_o[0]}]

# To ARD_D1 on Arduino 8-pin  Pin 2 = MOSI
set_property PACKAGE_PIN P8 [get_ports io0_io]
set_property IOSTANDARD LVCMOS33 [get_ports io0_io]

# To ARD_D2 on Arduino 8-pin  Pin 3 = MISO
set_property PACKAGE_PIN P9 [get_ports io1_io]
set_property IOSTANDARD LVCMOS33 [get_ports io1_io]

# To ARD_D3 on Arduino 8-pin  Pin 4 = SPI Clock
set_property PACKAGE_PIN R7 [get_ports sck_o]
set_property IOSTANDARD LVCMOS33 [get_ports sck_o]

set_property PULLUP true [get_ports {ss_o[0]}]
set_property PULLUP true [get_ports io0_io]
set_property PULLUP true [get_ports io1_io]
set_property PULLUP true [get_ports sck_o]
