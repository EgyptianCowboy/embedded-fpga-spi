/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include <stdio.h>

#define ON          0x00u
#define OFF         0x01u

#define BYTE_START  0x01u
#define BYTE_STOP   0x17u
#define BYTE_TOGW   0x05u

#define START_POS   0x00u
#define STOP_POS    0x02u
#define CMD_POS     0x01u

#define BYTE_OK     0x00u
#define BYTE_NOK    0xFFu

#define PACKET_SIZE 0x03u

const uint8 dummyBuffer[PACKET_SIZE] = {0x55u, 0x55u, 0x55u};

uint8 txBuffer[PACKET_SIZE] = {BYTE_START, BYTE_OK, BYTE_STOP};
uint8 tmpBuffer[PACKET_SIZE];
uint8 writeByte;
uint8 cmdByte;

uint16 capVal = 0;
uint16 capValOld = 0xFFFF;
char msg[10];

void ledWrite () {
    ledG_Write(ON);
    ledR_Write(OFF);
}

void ledNoWrite () {
    ledG_Write(OFF);
    ledR_Write(ON);
}

void initSys () {
    SPIS_Start();
    ledNoWrite();
    writeByte = 0;
    SPIS_SpiUartPutArray(dummyBuffer, PACKET_SIZE);
}

void readCap () {
        capVal = CapSense_GetCentroidPos(CapSense_LINEARSLIDER0__LS);
        if(capVal != 0xFFFF && capVal != capValOld) {
            sprintf(msg, "%3d\r\n", capVal);
            UART_UartPutString(msg);
            capValOld = capVal;
        }
        CapSense_UpdateEnabledBaselines();
        CapSense_ScanEnabledWidgets();
}

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    
    //initSys ();
    SPIS_Start();
    ledNoWrite();
    writeByte = 0;
    SPIS_SpiUartPutArray(txBuffer, PACKET_SIZE);
    UART_Start();
    UART_UartPutString("Start\n\r");
    CapSense_Start();
    CapSense_InitializeAllBaselines();
    CapSense_ScanEnabledWidgets();
    for(;;)
    {
        //UART_UartPutString("Before while\n\r");
        while(PACKET_SIZE != SPIS_SpiUartGetRxBufferSize());
        for(uint8 i = 0; i < PACKET_SIZE; i++) {
            tmpBuffer[i] = ((uint8)SPIS_SpiUartReadRxData() >> 1);
        }
        //SPIS_SpiUartClearTxBuffer();
        //SPIS_SpiUartClearRxBuffer();
        if(BYTE_START == tmpBuffer[START_POS] && BYTE_STOP == tmpBuffer[STOP_POS]) {
            cmdByte = tmpBuffer[CMD_POS];
            if(cmdByte == 0x05u) {
                //UART_UartPutString("Receiving cmd\n\r");
                writeByte = !writeByte;
                txBuffer[CMD_POS] = writeByte;
                SPIS_SpiUartPutArray(txBuffer, PACKET_SIZE);
                ledG_Write(!writeByte);
                ledR_Write(writeByte);
//                for(uint8 i = 0; i < PACKET_SIZE; i++) {
//                    sprintf(msg, "%3d\r\n", tmpBuffer[i]);
//                    UART_UartPutString(msg);
//                }
            } else if (writeByte) {
                //UART_UartPutString("Writing\n\r");
                if(!CapSense_IsBusy()) {
                     readCap ();
                }
                txBuffer[CMD_POS] = capVal;
                SPIS_SpiUartPutArray(txBuffer, PACKET_SIZE);
            } else {
                //UART_UartPutString("NOK\n\r");
                txBuffer[CMD_POS] = BYTE_NOK;
                SPIS_SpiUartPutArray(txBuffer, PACKET_SIZE);
            }
        } else {
            //UART_UartPutString("NOK2\n\r");
            txBuffer[CMD_POS] = BYTE_NOK;
            SPIS_SpiUartPutArray(txBuffer, PACKET_SIZE);
        }
        while(PACKET_SIZE != SPIS_SpiUartGetRxBufferSize());
        SPIS_SpiUartClearRxBuffer();
        SPIS_SpiUartPutArray(dummyBuffer, PACKET_SIZE);
    }   
}

/* [] END OF FILE */
