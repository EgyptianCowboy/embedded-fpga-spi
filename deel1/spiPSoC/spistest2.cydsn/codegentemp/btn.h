/*******************************************************************************
* File Name: btn.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_btn_H) /* Pins btn_H */
#define CY_PINS_btn_H

#include "cytypes.h"
#include "cyfitter.h"
#include "btn_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} btn_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   btn_Read(void);
void    btn_Write(uint8 value);
uint8   btn_ReadDataReg(void);
#if defined(btn__PC) || (CY_PSOC4_4200L) 
    void    btn_SetDriveMode(uint8 mode);
#endif
void    btn_SetInterruptMode(uint16 position, uint16 mode);
uint8   btn_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void btn_Sleep(void); 
void btn_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(btn__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define btn_DRIVE_MODE_BITS        (3)
    #define btn_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - btn_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the btn_SetDriveMode() function.
         *  @{
         */
        #define btn_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define btn_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define btn_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define btn_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define btn_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define btn_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define btn_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define btn_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define btn_MASK               btn__MASK
#define btn_SHIFT              btn__SHIFT
#define btn_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in btn_SetInterruptMode() function.
     *  @{
     */
        #define btn_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define btn_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define btn_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define btn_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(btn__SIO)
    #define btn_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(btn__PC) && (CY_PSOC4_4200L)
    #define btn_USBIO_ENABLE               ((uint32)0x80000000u)
    #define btn_USBIO_DISABLE              ((uint32)(~btn_USBIO_ENABLE))
    #define btn_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define btn_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define btn_USBIO_ENTER_SLEEP          ((uint32)((1u << btn_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << btn_USBIO_SUSPEND_DEL_SHIFT)))
    #define btn_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << btn_USBIO_SUSPEND_SHIFT)))
    #define btn_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << btn_USBIO_SUSPEND_DEL_SHIFT)))
    #define btn_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(btn__PC)
    /* Port Configuration */
    #define btn_PC                 (* (reg32 *) btn__PC)
#endif
/* Pin State */
#define btn_PS                     (* (reg32 *) btn__PS)
/* Data Register */
#define btn_DR                     (* (reg32 *) btn__DR)
/* Input Buffer Disable Override */
#define btn_INP_DIS                (* (reg32 *) btn__PC2)

/* Interrupt configuration Registers */
#define btn_INTCFG                 (* (reg32 *) btn__INTCFG)
#define btn_INTSTAT                (* (reg32 *) btn__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define btn_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(btn__SIO)
    #define btn_SIO_REG            (* (reg32 *) btn__SIO)
#endif /* (btn__SIO_CFG) */

/* USBIO registers */
#if !defined(btn__PC) && (CY_PSOC4_4200L)
    #define btn_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define btn_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define btn_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define btn_DRIVE_MODE_SHIFT       (0x00u)
#define btn_DRIVE_MODE_MASK        (0x07u << btn_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins btn_H */


/* [] END OF FILE */
